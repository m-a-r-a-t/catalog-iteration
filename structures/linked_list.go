package structures

import (
	"strings"
)

type nodeDouble[T any] struct {
	data T
	next *nodeDouble[T]
	prev *nodeDouble[T]
}

func (n *nodeDouble[T]) Next() *nodeDouble[T] {
	return n.next
}

type DoubleLinkedListI[T any] interface {
	Head() *nodeDouble[T]
	InsertTail(item T)
	DropTail()
	Size() int
	Clear()
}

type DoubleLinkedList[T any] struct {
	head *nodeDouble[T]
	tail *nodeDouble[T]
	size int
}

func (dl *DoubleLinkedList[T]) Head() *nodeDouble[T] {
	return dl.head
}

func (dl *DoubleLinkedList[T]) InsertTail(item T) {
	if dl.head == nil {
		dl.head = &nodeDouble[T]{item, nil, nil}
		dl.tail = dl.head
	} else {
		newTail := &nodeDouble[T]{data: item, next: nil, prev: dl.tail}
		dl.tail.next = newTail
		dl.tail = newTail
	}
	dl.size++
}

func (dl *DoubleLinkedList[T]) PopTail() {
	if dl.head == dl.tail {
		dl.head = nil
		dl.tail = nil
		dl.size = 0
	} else {
		// prev := dl.tail.prev
		// prev.next = nil
		// dl.tail = prev

		dl.tail = dl.tail.prev
		dl.tail.next = nil
		dl.size--
	}

}

func (dl *DoubleLinkedList[T]) Size() int {
	return dl.size
}

func NewDirPath() *DirPath {

	return &DirPath{}
}

type DirPath struct {
	strBuilder strings.Builder
	DoubleLinkedList[string]
}

func (d *DirPath) GetPath() string {
	defer d.strBuilder.Reset()

	v := d.Head()
	if v != nil {
		if v.data == "" {
			d.strBuilder.WriteString("./")
		} else {
			if v.data[0] == '/' {
				d.strBuilder.WriteString(v.data)
			} else {
				d.strBuilder.WriteString("./")
				d.strBuilder.WriteString(v.data)
			}
		}

		for v = v.Next(); v != nil; v = v.Next() {
			d.strBuilder.WriteRune('/')
			d.strBuilder.WriteString(v.data)
		}

	}

	return d.strBuilder.String()
}
