package structures

type node[T any] struct {
	data T
	next *node[T]
}

type Stack[T any] interface {
	Push(item T)
	Pop() (T, bool)
	IsEmpty() bool
	GetTop() (T, bool)
	Size() int
	Clear()
}

type StackLinkedList[T any] struct {
	top  *node[T]
	size int
}

func NewStack[T any]() Stack[T] {
	return &StackLinkedList[T]{}
}

func (s *StackLinkedList[T]) Size() int {
	return s.size
}

func (s *StackLinkedList[T]) Clear() {
	s.top = nil
	s.size = 0
}

func (s *StackLinkedList[T]) Push(item T) {
	if s.top != nil {
		next := s.top
		s.top = &node[T]{item, next}

	} else {
		s.top = &node[T]{item, nil}
	}
	s.size++
}

func (s *StackLinkedList[T]) GetTop() (T, bool) {

	if s.top != nil {

		return s.top.data, true
	}

	return *new(T), false
}

func (s *StackLinkedList[T]) Pop() (T, bool) {

	if s.top != nil {
		top := s.top
		s.top = top.next
		s.size--
		return top.data, true
	}

	return *new(T), false
}

func (s *StackLinkedList[T]) IsEmpty() bool {
	return s.top == nil
}
