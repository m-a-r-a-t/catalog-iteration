package main

import (
	"bufio"
	"catalog-iteration/structures"
	"os"
)

func main() {
	var path string
	if len(os.Args) >= 2 {
		path = os.Args[1]
	}

	file, err := os.Create("result.txt")
	if err != nil {

		panic(err)
	}

	defer file.Close()

	writer := bufio.NewWriterSize(file, 4096)

	stack := structures.NewStack[string]()
	dirPath := structures.NewDirPath()

	dfs(path, writer, stack, dirPath)

	writer.Flush()

}

func dfs(root string, w *bufio.Writer, stack structures.Stack[string], dirPath *structures.DirPath) {
	var j, deepCount, prevStackSize int
	var curDirPath, curDirName, p string
	visited := make(map[string]struct{}, 3000)

	stack.Push(root)

	for !stack.IsEmpty() {
		curDirName, _ = stack.GetTop()
		p = dirPath.GetPath() + "/" + curDirName
		_, err := os.ReadDir(p)

		if err != nil {
			_, err := os.Stat(p)
			if err != nil {
				if !os.IsNotExist(err) {
					stack.Pop()
				}
				deepCount--
				dirPath.PopTail()
				continue
			}
		}

		dirPath.InsertTail(curDirName)

		curDirPath = dirPath.GetPath()

		if _, ok := visited[curDirPath]; ok {
			stack.Pop()
			dirPath.PopTail()
			continue
		}

		visited[curDirPath] = struct{}{}

		stack.Pop()

		writeWithIndents(w, curDirName, deepCount, true)

		dirEntries, _ := os.ReadDir(dirPath.GetPath())

		if len(dirEntries) == 0 {
			dirPath.PopTail()
			continue
		}

		deepCount++

		prevStackSize = stack.Size()

		for j = 0; j < len(dirEntries); j++ {

			if !dirEntries[j].IsDir() {
				writeWithIndents(w, dirEntries[j].Name(), deepCount, false)
				continue
			}

			if _, ok := visited[curDirPath+"/"+dirEntries[j].Name()]; !ok {
				stack.Push(dirEntries[j].Name())
			}

		}

		if stack.Size() == prevStackSize {
			dirPath.PopTail()
			deepCount--
		}

	}

}

func writeWithIndents(w *bufio.Writer, s string, deepCount int, isDir bool) {
	for i := 0; i < deepCount; i++ {
		w.WriteString("   ")
	}

	w.WriteString(s)
	if isDir {
		if s[len(s)-1] != '/' {
			w.WriteString("/")
		}
	}
	w.WriteString("\n")
}

/*
Тестовое задание:
Исключительно на GO.
Сделать обход каталога (все файловое дерево - вся структура) большой вложенности
с большим объемом файлов (1 млрд) не нагружающий дисковое IO с минимальными затратами ресурсов.
Результат записать в файл.
*/
